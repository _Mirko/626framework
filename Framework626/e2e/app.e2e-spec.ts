import { Framework626Page } from './app.po';

describe('framework626 App', () => {
  let page: Framework626Page;

  beforeEach(() => {
    page = new Framework626Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
